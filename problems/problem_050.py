# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

from math import ceil


def halve_the_list(list):

    half = ceil(len(list)/2)
    list1 = []
    list2 = []

    for number in list:
        if len(list1) < half:
            list1.append(number)
    for number in list:
        if number not in list1:
            list2.append(number)

    return list1, list2

print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([1, 2, 3]))
