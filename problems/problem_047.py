# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):

    special_char = ["$","!","@"]
    lower_case_count = 0
    upper_case_count = 0
    digit_count = 0
    special_char_count = 0


    for char in password:
        if char.islower():
            lower_case_count +=1
        elif char.isupper():
            upper_case_count +=1
        elif char.isdigit():
            digit_count+=1
        elif char in special_char:
            special_char_count +=1

    print(lower_case_count)
    print(upper_case_count)
    print(digit_count)
    print(special_char_count)

    if (lower_case_count >= 1 and
        upper_case_count >= 1 and
        digit_count >= 1 and
        special_char_count >= 1 and
        len(password)>=6 and
        len(password) <=12):
        return True
    else:
        return False

print(check_password("Hello123@"))
